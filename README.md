#### Starta frontend
```
cd frontend
npm install
npm start
```

#### Starta backend
```
cd backend
./gradlew clean build

docker-compose up (to start DB) 
java -jar build/libs/mh-0.0.1-SNAPSHOT.jar
eller
 ./gradlew bootRun

backend controller endpoint -  t.ex http://localhost:8089/lab-mh-ch/v1/its/1000/switch/true  (iterations : 1000 , switch : true)
```

#### Verifikation
React appen har en komponent som pollar spring boot appens health endpoint och skriver ut svaret. Svaret ska vara "UP" om allt funkar som det ska.
