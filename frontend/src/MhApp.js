import React, { Component } from 'react';

class MhApp extends Component {

    constructor(props) {
        super(props);
        this.state = {
            switch: true,
            numberOfIterations: 1000,
            result : 0
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
    }

    handleInputChange(event) {

        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        console.log("handle change ")
        console.log(event.target.value)
        this.setState({
            [name]: value
        });
    }

    handleFormSubmit(e) {
        e.preventDefault();
        let choice = this.state.switch;
        let iterations = this.state.numberOfIterations;

        console.log(choice)
        console.log(iterations)

        fetch(`/its/${iterations}/switch/${choice}`, {
            method: "GET",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            }
        }).then(response => {
            response.json().then(data => {
                console.log("Successful - " + data);
                this.setState({
                    result: data
                })
            });
        }).catch( ex => {
            console.log(ex)
        });

    }
    render() {
        return (
            <form onSubmit={this.handleFormSubmit}>
                <label>
                    switch:
                    <input
                        name="switch"
                        type="checkbox"
                        checked={this.state.switch}
                        onChange={this.handleInputChange} />
                </label>
                <br />
                <label>
                    Iterations:
                    <input
                        name="numberOfIterations"
                        type="number"
                        value={this.state.numberOfIterations}
                        onChange={this.handleInputChange} />
                </label>
                <br />
                <input type="submit" value="Submit" onSubmit={this.handleFormSubmit}/>
                <br/>

                <label>
                    output: {this.state.result}
                </label>
            </form>
        );
    }
}

export default MhApp