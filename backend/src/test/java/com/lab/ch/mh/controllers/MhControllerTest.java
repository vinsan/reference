package com.lab.ch.mh.controllers;

import com.lab.ch.mh.service.MHSimService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@WebMvcTest(MhController.class)
public class MhControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    MHSimService mhSimService;

    @Test
    public void mhControllerTest() throws Exception {

        Mockito.when(mhSimService.recursionPlay(Mockito.anyLong(), Mockito.anyBoolean())).thenReturn(67L);

        RequestBuilder request = MockMvcRequestBuilders.get("/its/100/switch/true").accept(MediaType.APPLICATION_JSON);

        MvcResult mvcResult = mockMvc.perform(request).andReturn();


       // Assert.assertEquals(67L, Long.parseLong(mvcResult.getResponse().getContentAsString()));
        Assert.assertEquals(200, mvcResult.getResponse().getStatus());

    }
}
