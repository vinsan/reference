package com.lab.ch.mh.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "stats")
public class ProbableWinEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer play_id;

  public ProbableWinEntity() {
  }

  private long iterations;

  private Boolean choice;

  private long wins;

  public ProbableWinEntity(long iterations, Boolean choice, long wins) {
    super();
    this.iterations = iterations;
    this.choice = choice;
    this.wins = wins;
  }
}
