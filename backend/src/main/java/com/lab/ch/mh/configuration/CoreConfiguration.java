package com.lab.ch.mh.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class CoreConfiguration {
    @Bean
    public WebMvcConfigurer corsConfigurer() {

        //return new WebMvcConfigurer(){
        //    @Override
        //    public void addCorsMappings(CorsRegistry registry) {
        //        registry.addMapping("/greeting-javaconfig").allowedOrigins("http://localhost:3000");
        //    }
        //};
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/greeting-javaconfig").allowedOrigins("http://localhost:3000");
            }
        };
    }
}
