package com.lab.ch.mh.repositories;

import com.lab.ch.mh.entities.ProbableWinEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProbableWinsRepository extends JpaRepository<ProbableWinEntity, Integer> {


}
