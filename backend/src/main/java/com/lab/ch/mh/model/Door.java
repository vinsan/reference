package com.lab.ch.mh.model;

import lombok.AllArgsConstructor;

import java.util.Objects;

@AllArgsConstructor
public class Door {

    private final int door;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Door providedDoor = (Door) o;
        return door == providedDoor.door;
    }

    @Override
    public int hashCode() {
        return Objects.hash(door);
    }
}
