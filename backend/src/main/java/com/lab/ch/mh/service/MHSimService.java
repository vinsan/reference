package com.lab.ch.mh.service;

import com.lab.ch.mh.entities.ProbableWinEntity;
import com.lab.ch.mh.model.Door;
import com.lab.ch.mh.repositories.ProbableWinsRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Random;
import java.util.stream.LongStream;

@Slf4j
@Service
public class MHSimService {

    private final ProbableWinsRepository repository;

    public MHSimService(ProbableWinsRepository repository) {
        this.repository = repository;
    }

    public long recursionPlay(long iterations, Boolean s){

        long wins = LongStream
            .rangeClosed(1, iterations)
            .mapToObj(n -> game(s))
            .filter(r -> r.equals(RESULT.WIN))
            .count();

        repository.save(toEntity(iterations,s,wins));

        return  wins;

    }

    public RESULT game(Boolean switchChoice) {

        // selecting a winning door
        Door winningDoor = new Door(new Random().nextInt(3) + 1);

        // first chosen door
        Door chosenDoor = new Door(new Random().nextInt(3) + 1);

        // shown door should  be anything but   ~chosen or winning
        Door shownDoor = new Door(new Random().nextInt(3)+1);

        while(shownDoor.equals(chosenDoor) || shownDoor.equals(winningDoor)){
            shownDoor = new Door(new Random().nextInt(3)+1);
        }

        if(switchChoice) {
            Door newChosenDoor = new Door(new Random().nextInt(3)+1);
            while(newChosenDoor.equals(chosenDoor) || newChosenDoor.equals(shownDoor)){
                newChosenDoor = new Door(new Random().nextInt(3)+1);
            }
            chosenDoor = newChosenDoor;
        }

        if(winningDoor.equals(chosenDoor)){
            return RESULT.WIN;
        }else{
            return RESULT.LOOSE;
        }
    }
    enum RESULT {
        WIN,
        LOOSE;
    }



    private ProbableWinEntity toEntity(long iterations, Boolean choice, long wins){

        ProbableWinEntity probableWinEntity = new ProbableWinEntity(iterations, choice,wins);
        return probableWinEntity;
    }
}