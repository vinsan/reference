package com.lab.ch.mh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication
public class MhApplication {

    public static void main(String[] args) {
        SpringApplication.run(MhApplication.class, args);
    }
}
