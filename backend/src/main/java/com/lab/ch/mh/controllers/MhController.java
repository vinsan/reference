package com.lab.ch.mh.controllers;

import com.lab.ch.mh.service.MHSimService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MhController {

    MHSimService service;

    public MhController(MHSimService service) {
        this.service = service;
    }

    @CrossOrigin
    @GetMapping(path = "/its/{its}/switch/{s}", produces = MediaType.APPLICATION_JSON_VALUE)
    public long getProbabilty(@PathVariable final long its, @PathVariable final boolean s){
        return service.recursionPlay(its, s);
    }

}
