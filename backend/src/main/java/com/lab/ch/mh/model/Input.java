package com.lab.ch.mh.model;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Input {

    private final Long iterations;
    private final Boolean switchDoor;
}
